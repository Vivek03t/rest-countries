import React, { useContext, useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { StatesContext } from '../context/ContextProvider';
import { AiOutlineArrowLeft } from 'react-icons/ai';

function DetailsInfoCard({ countryDetails }) {
  const { isDarkMode, setError } = useContext(StatesContext);
  const [allCountries, setAllCountries] = useState([]);

  const { id } = useParams();
  let countryCodeMap = {};

  const URL = `https://restcountries.com/v3.1/all`;

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(URL);
        if (!response.ok) {
          throw new Error(`HTTP Error! Status: ${response.status}`);
        }

        const data = await response.json();

        setAllCountries(data);
      } catch (error) {
        console.log(error);
        setError(error);
      }
    };

    fetchData();
  }, [id]);

  if (allCountries.length > 0) {
    allCountries.map((ele) => {
      countryCodeMap[ele.cca3] = ele.name.common;
    });
  }

  const navigate = useNavigate();

  const navigateToHome = () => {
    navigate(-1);
  };

  const navigateToCountry = (id) => {
    navigate(`/country/${id}`);
  };

  const {
    name,
    population,
    flags,
    region,
    subregion,
    capital,
    tld,
    currencies,
    languages,
    borders,
  } = countryDetails[0];

  let currency = currencies ? Object.keys(currencies) : [];
  currency = currency.map((ele) => ele).join(',');

  let language = languages ? Object.values(languages).join(',') : [];

  const detailsClassName = ` ${isDarkMode ? 'dark-mode' : ''}`;
  const textClassName = `country-info ${isDarkMode ? 'dark-text' : ''}`;

  return (
    <div className={`country-container ${detailsClassName}`}>
      <button
        className={`btn btn-outline-dark back-button ${
          isDarkMode ? 'dark-mode' : ''
        }`}
        type='submit'
        onClick={navigateToHome}
      >
        <span className={`back-button ${textClassName} `}>
          <span className='back-icon'>
            <AiOutlineArrowLeft />
          </span>
          Back
        </span>
      </button>

      <div className='country-info-container'>
        <div className='image-section'>
          <img src={flags.svg} alt={`${name?.common}-flag`} />
        </div>
        <div className='info-section'>
          <div className={textClassName}>
            <h4>
              <span>{name?.common}</span>
            </h4>
          </div>
          <div className='info-container'>
            <div className='left-info-section'>
              <div className={textClassName}>
                Native Name:
                <span>
                  {name.nativeName && Object.values(name.nativeName)[0].common}
                </span>
              </div>
              <div className={textClassName}>
                Population: <span>{population}</span>
              </div>
              <div className={textClassName}>
                Region: <span>{region}</span>
              </div>
              <div className={textClassName}>
                Sub Region: <span>{subregion}</span>
              </div>
              <div className={textClassName}>
                Capital: <span>{capital}</span>
              </div>
            </div>
            <div className='right-info-section'>
              <div className={textClassName}>
                Top Level Domain: <span>{tld}</span>
              </div>
              <div className={textClassName}>
                Currencies: <span>{currency}</span>
              </div>
              <div className={textClassName}>
                Languages: <span>{language}</span>
              </div>
            </div>
          </div>
          <div className={`borders ${textClassName}`}>
            Border Countries:
            {borders !== undefined
              ? borders.map((element) => (
                  <button
                    className={`btn btn-outline-dark border ${detailsClassName} ${
                      isDarkMode ? 'dark-text' : ''
                    }`}
                    type='submit'
                    onClick={() => navigateToCountry(element)}
                  >
                    <span
                      className={`border-country ${
                        isDarkMode ? 'dark-text' : ''
                      }`}
                    >
                      {countryCodeMap[element]}
                    </span>
                  </button>
                ))
              : []}
          </div>
        </div>
      </div>
    </div>
  );
}

export default DetailsInfoCard;
