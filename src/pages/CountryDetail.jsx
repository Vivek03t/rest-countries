import React, { useState, useEffect, useContext } from 'react';
import { useParams } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { StatesContext } from '../context/ContextProvider';
import Loader from '../components/Loader';
import DetailsInfoCard from './DetailsInfoCard';

function CountryDetail() {
  const { error, setError } = useContext(StatesContext);
  const { id } = useParams();

  const [countryDetails, setCountryDetails] = useState([]);
  const [isLoading, setLoader] = useState(true);

  const URL = `https://restcountries.com/v3.1/alpha/${id}`;

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(URL);
        if (!response.ok) {
          throw new Error(`HTTP Error! Status: ${response.status}`);
        }

        const data = await response.json();
        setCountryDetails(data);
        setLoader(false);
      } catch (error) {
        console.log(error);
        setError(error);
      }
    };

    fetchData();
  }, [id]);

  return (
    <div className='loader-img'>
      {isLoading ? (
        <Loader />
      ) : error ? (
        <div className='error-message'>
          <p>An error occurred: {error.message}</p>
        </div>
      ) : (
        <DetailsInfoCard countryDetails={countryDetails} />
      )}
    </div>
  );
}

export default CountryDetail;
