import React, { useEffect, useContext } from 'react';
import '../App.css';
import { StatesContext } from '../context/ContextProvider';
import Actions from '../components/actions/Actions';
import Countries from '../components/Countries';
import Loader from '../components/Loader';

function Home() {
  const { isDarkMode, isLoading, setLoader, error, setError, setCountryData } =
    useContext(StatesContext);

  const URL = 'https://restcountries.com/v3.1/all';

  useEffect(() => {
    setLoader(true);
    const fetchData = async () => {
      try {
        const response = await fetch(URL);
        if (!response.ok) {
          throw new Error(`HTTP Error! Status: ${response.status}`);
        }

        const data = await response.json();
        setCountryData(data);
        setTimeout(() => {
          setLoader(false);
        }, 1000);
      } catch (error) {
        console.log(error);
        setError(error);
      }
    };

    fetchData();
  }, []);

  return (
    <div className={`main__container ${isDarkMode ? 'dark-mode' : ''}`}>
      <main className={`actions-container ${isDarkMode ? 'dark-mode' : ''}`}>
        <Actions />
      </main>
      <div className='loader-img'>
        {isLoading ? (
          <Loader />
        ) : error ? (
          <div className='error-message'>
            <p>An error occurred: {error.message}</p>
          </div>
        ) : (
          <Countries />
        )}
      </div>
    </div>
  );
}

export default Home;
