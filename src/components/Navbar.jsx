import React, { useContext } from 'react';
import { BsMoon, BsMoonFill } from 'react-icons/bs';
import { StatesContext } from '../context/ContextProvider';

function Navbar() {
  const { isDarkMode, setDarkMode } = useContext(StatesContext);

  const toggleDarkMode = () => {
    setDarkMode(!isDarkMode);
  };
  const navbarClassName = `navbar bg-body-tertiary ${
    isDarkMode ? 'dark-mode' : ''
  }`;

  return (
    <>
      <nav className={navbarClassName}>
        <div className='container-fluid'>
          <a className={`navbar-brand ${isDarkMode ? 'dark-text' : ''}`}>
            Where in the World?
          </a>
          <button
            className='btn btn-outline'
            type='submit'
            onClick={toggleDarkMode}
          >
            {isDarkMode ? (
              <BsMoonFill
                className={`icon-moon ${isDarkMode ? 'dark-text' : ''}`}
              />
            ) : (
              <BsMoon
                className={`icon-moon ${isDarkMode ? 'dark-text' : ''}`}
              />
            )}
            <span className={`text ${isDarkMode ? 'dark-text' : ''}`}>
              Dark Mode
            </span>
          </button>
        </div>
      </nav>
    </>
  );
}

export default Navbar;
