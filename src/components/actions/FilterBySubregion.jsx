import React, { useState, useContext } from 'react';
import { StatesContext } from '../../context/ContextProvider';

function FilterBySubregion() {
  const { countryData, setSubregion, isDarkMode, region, setRegion } =
    useContext(StatesContext);
  const [subregionOpen, setSubregionOpen] = useState(false);
  const [selectedSubregion, setSelectedSubregion] = useState('');

  let countries = [...countryData];
  let subregionsSet = new Set();
  countries.forEach((country) => {
    if (country.region === region) {
      subregionsSet.add(country.subregion);
    }
  });

  let subregionsArray = Array.from(subregionsSet);

  const handleRegion = (newRegion) => {
    setRegion(newRegion);

    if (newRegion !== region) {
      setSubregion('');
      setSelectedSubregion('');
    }

    setSubregionOpen(false);
  };

  const handleSubregion = (subregion) => {
    setSelectedSubregion(subregion);
    setSubregion(subregion);
    setSubregionOpen(false);
  };

  return (
    <div className='input-group-append'>
      <button
        className={`btn btn-outline-secondary dropdown-toggle ${
          isDarkMode ? 'dark-mode' : ''
        }`}
        type='button'
        data-toggle='dropdown'
        style={{ color: isDarkMode ? 'white' : 'black' }}
        onClick={() => setSubregionOpen(!subregionOpen)}
      >
        {selectedSubregion || region || 'Select Subregion'}
      </button>
      <div
        className={`dropdown-menu ${subregionOpen ? 'show' : ''} ${
          isDarkMode ? 'dark-mode' : ''
        } ${isDarkMode ? 'dark-text' : ''}`}
      >
        <button
          onClick={() => handleRegion('')}
          className={`dropdown-item ${isDarkMode ? 'dark-text' : ''}`}
        >
          All
        </button>
        {subregionsArray.length > 0 &&
          subregionsArray.map((subregionName, index) => (
            <button
              key={index}
              onClick={() => handleSubregion(subregionName)}
              className={`dropdown-item ${isDarkMode ? 'dark-text' : ''}`}
            >
              {subregionName}
            </button>
          ))}
      </div>
    </div>
  );
}

export default FilterBySubregion;
