import React, { useState, useContext } from 'react';
import { StatesContext } from '../../context/ContextProvider';

function SortByArea() {
  const { isDarkMode, sortByArea, setSortByArea } = useContext(StatesContext);
  const [sortOpen, setSortOpen] = useState(false);
  const [selectedSort, setSelectedSort] = useState('Sort by Area');

  const handleSort = (sortType) => {
    if (sortType === 'Sort by Area' && sortByArea === 'Sort by Area') {
      setSelectedSort('Sort by Area');
      setSortByArea(null);
    } else {
      setSelectedSort(sortType);
      setSortByArea(sortType);
    }
    setSortOpen(false);
  };

  return (
    <div className='input-group-append'>
      <button
        className={`btn btn-outline-secondary dropdown-toggle ${
          isDarkMode ? 'dark-mode' : ''
        }`}
        type='button'
        data-toggle='dropdown'
        style={{ color: isDarkMode ? 'white' : 'black' }}
        onClick={() => setSortOpen(!sortOpen)}
      >
        {selectedSort}
      </button>
      <div
        className={`dropdown-menu ${sortOpen ? 'show' : ''} ${
          isDarkMode ? 'dark-mode' : ''
        } ${isDarkMode ? 'dark-text' : ''}`}
      >
        <button
          onClick={() => handleSort('Sort by Area')}
          className={`dropdown-item ${isDarkMode ? 'dark-text' : ''}`}
        >
          Sort by Area
        </button>
        <button
          onClick={() => handleSort('Ascending')}
          className={`dropdown-item ${isDarkMode ? 'dark-text' : ''}`}
        >
          Ascending
        </button>
        <button
          onClick={() => handleSort('Descending')}
          className={`dropdown-item ${isDarkMode ? 'dark-text' : ''}`}
        >
          Descending
        </button>
      </div>
    </div>
  );
}

export default SortByArea;
