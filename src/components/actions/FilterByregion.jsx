import React, { useState, useContext } from 'react';
import { StatesContext } from '../../context/ContextProvider';

function FilterByregion() {
  const { setRegion, setSubregion, isDarkMode } = useContext(StatesContext);
  const [isOpen, setIsOpen] = useState(false);
  const [selectedRegion, setSelectedRegion] = useState('');

  const displayText = selectedRegion ? selectedRegion : 'Filter By Region';

  const dropdownClassName = `btn btn-outline-secondary dropdown-toggle ${
    isDarkMode ? 'dark-mode' : ''
  }`;

  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };

  const handleRegion = (region) => {
    setSubregion('');
    setSelectedRegion(region);
    setRegion(region);
    setIsOpen(false);
  };

  return (
    <div className='input-group-append'>
      <button
        className={dropdownClassName}
        type='button'
        data-toggle='dropdown'
        style={{ color: isDarkMode ? 'white' : 'black' }}
        onClick={toggleDropdown}
      >
        {displayText}
      </button>
      <div
        className={`dropdown-menu ${isOpen ? 'show' : ''} ${
          isDarkMode ? 'dark-mode' : ''
        } ${isDarkMode ? 'dark-text' : ''}`}
      >
        <a
          onClick={() => handleRegion('')}
          className={`dropdown-item ${!selectedRegion ? 'active' : ''} ${
            isDarkMode ? 'dark-text' : ''
          }`}
          href='#'
        >
          All
        </a>
        <a
          onClick={() => handleRegion('Oceania')}
          className={`dropdown-item ${
            selectedRegion === 'Oceania' ? 'active' : ''
          } ${isDarkMode ? 'dark-text' : ''}`}
          href='#'
        >
          Oceania
        </a>
        <a
          onClick={() => handleRegion('Americas')}
          className={`dropdown-item ${
            selectedRegion === 'Americas' ? 'active' : ''
          } ${isDarkMode ? 'dark-text' : ''}`}
          href='#'
        >
          Americas
        </a>
        <a
          onClick={() => handleRegion('Africa')}
          className={`dropdown-item ${
            selectedRegion === 'Africa' ? 'active' : ''
          } ${isDarkMode ? 'dark-text' : ''}`}
          href='#'
        >
          Africa
        </a>
        <a
          onClick={() => handleRegion('Europe')}
          className={`dropdown-item ${
            selectedRegion === 'Europe' ? 'active' : ''
          } ${isDarkMode ? 'dark-text' : ''}`}
          href='#'
        >
          Europe
        </a>
        <a
          onClick={() => handleRegion('Asia')}
          className={`dropdown-item ${
            selectedRegion === 'Asia' ? 'active' : ''
          } ${isDarkMode ? 'dark-text' : ''}`}
          href='#'
        >
          Asia
        </a>
        <a
          onClick={() => handleRegion('Antarctic')}
          className={`dropdown-item ${
            selectedRegion === 'Antarctic' ? 'active' : ''
          } ${isDarkMode ? 'dark-text' : ''}`}
          href='#'
        >
          Antarctic
        </a>
      </div>
    </div>
  );
}

export default FilterByregion;
