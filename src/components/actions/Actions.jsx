import { useContext } from 'react';
import { PiMagnifyingGlassLight } from 'react-icons/pi';
import { StatesContext } from '../../context/ContextProvider';
import FilterByregion from './FilterByregion';
import FilterBySubregion from './FilterBySubregion';
import SortByArea from './SortByArea';
import SortByPopulation from './SortByPopulation';

function Actions() {
  const { setSearch, isDarkMode } = useContext(StatesContext);

  return (
    <>
      <div className={`actions ${isDarkMode ? 'dark-mode' : ''}`}>
        <div className='input-group'>
          <div
            className={`input-group-search ${isDarkMode ? 'dark-mode' : ''}`}
          >
            <span className='input-group-icon'>
              <PiMagnifyingGlassLight
                className={`icon-search ${isDarkMode ? 'dark-text' : ''}`}
              />
            </span>
            <input
              type='text'
              className={`form-control ${isDarkMode ? 'dark-mode' : ''} ${
                isDarkMode ? 'dark-text' : ''
              }`}
              onChange={(e) => setSearch(e.target.value)}
              placeholder='Search for a country...'
              aria-label='Search'
            />
          </div>

          <FilterByregion />
          <FilterBySubregion />
          <SortByArea />
          <SortByPopulation />
        </div>
      </div>
    </>
  );
}

export default Actions;
