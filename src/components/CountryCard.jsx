import React, { useContext } from 'react';
import { StatesContext } from '../context/ContextProvider';
import { useNavigate } from 'react-router-dom';

function CountryCard({ country }) {
  const { isDarkMode } = useContext(StatesContext);
  const cardClassName = `country-card ${isDarkMode ? 'dark-mode' : ''}`;
  const textClassName = `title ${isDarkMode ? 'dark-text' : ''}`;

  const navigate = useNavigate();

  const handleClick = () => {
    navigate(`./country/${country.cca3}`);
  };

  return (
    <div className={cardClassName} onClick={handleClick}>
      <div className='country-img'>
        <img src={country.flags.png} alt={country.name.common} />
      </div>
      <div className='info'>
        <h1 className={textClassName}>{country.name.common}</h1>
        <h3>
          <span className={textClassName}>Population:</span>
          <span>{country.population}</span>
        </h3>
        <h3>
          <span className={textClassName}>Region:</span>
          <span>{country.region}</span>
        </h3>
        <h3>
          <span className={textClassName}>Capital:</span>
          <span>{country.capital}</span>
        </h3>
      </div>
    </div>
  );
}

export default CountryCard;
