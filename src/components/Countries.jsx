import React, { useContext } from 'react';
import CountryCard from './CountryCard';
import { StatesContext } from '../context/ContextProvider';
import Error from './Error';

function Countries() {
  const {
    countryData,
    search,
    region,
    subregion,
    isDarkMode,
    sortByPopulation,
    sortByArea,
  } = useContext(StatesContext);
  const containerClassName = `countries-container ${
    isDarkMode ? 'dark-mode' : ''
  }`;

  const filteredData = countryData.filter((country) => {
    const countryName = country.name.common.toLowerCase();
    const regionName = country.region.toLowerCase();
    const isSearchMatch =
      search === '' || countryName.includes(search.toLowerCase());
    const isRegionMatch =
      region === '' || regionName.includes(region.toLowerCase());

    return isSearchMatch && isRegionMatch;
  });

  const sortData = (data) => {
    if (sortByPopulation === 'Ascending') {
      return data.sort((a, b) => a.population - b.population);
    } else if (sortByPopulation === 'Descending') {
      return data.sort((a, b) => b.population - a.population);
    } else if (sortByArea === 'Ascending') {
      return data.sort((a, b) => a.area - b.area);
    } else if (sortByArea === 'Descending') {
      return data.sort((a, b) => b.area - a.area);
    } else {
      return data;
    }
  };

  const filteredBySubregion = filteredData.filter((country) => {
    return subregion === '' || country.subregion === subregion;
  });

  const sortedCountries = sortData(filteredBySubregion);

  return (
    <div className={containerClassName}>
      {sortedCountries.length > 0 ? (
        sortedCountries.map((country, index) => {
          return <CountryCard key={index} country={country} />;
        })
      ) : (
        <Error error='No such countries found' />
      )}
    </div>
  );
}

export default Countries;
