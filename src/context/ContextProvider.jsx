import { createContext, useState } from 'react';

export const StatesContext = createContext();

function ContextProvider({ children }) {
  const [search, setSearch] = useState('');
  const [region, setRegion] = useState('');
  const [subregion, setSubregion] = useState('');
  const [countryData, setCountryData] = useState([]);
  const [isDarkMode, setDarkMode] = useState(false);
  const [isLoading, setLoader] = useState(true);
  const [error, setError] = useState(null);
  const [sortByPopulation, setSortByPopulation] = useState('');
  const [sortByArea, setSortByArea] = useState('');

  return (
    <StatesContext.Provider
      value={{
        isDarkMode,
        setDarkMode,
        isLoading,
        setLoader,
        countryData,
        setCountryData,
        search,
        setSearch,
        region,
        setRegion,
        subregion,
        setSubregion,
        sortByArea,
        sortByPopulation,
        setSortByArea,
        setSortByPopulation,
        error,
        setError,
      }}
    >
      <div className={`main__container ${isDarkMode ? 'dark-mode' : ''}`}>
        {children}
      </div>
    </StatesContext.Provider>
  );
}

export default ContextProvider;
