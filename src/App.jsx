import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import ContextProvider from './context/ContextProvider';
import Home from './pages/Home';
import CountryDetail from './pages/CountryDetail';
import Navbar from './components/Navbar';

function App() {
  return (
    <ContextProvider>
      <BrowserRouter>
        <Navbar />
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='country/:id' element={<CountryDetail />} />
        </Routes>
      </BrowserRouter>
    </ContextProvider>
  );
}

export default App;
