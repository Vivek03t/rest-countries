# Rest Countries app 


This is a simple web application built using React to display information about different countries using data from the Rest Countries API. The project utilizes Bootstrap components and HTML, CSS, and JavaScript for creating a responsive and interactive user interface.

## Live Demo

Check out the live demo of this project [https://rest-countries-ten-psi.vercel.app/](https://rest-countries-ten-psi.vercel.app/).

## GitLab Repository

You can find the project's source code on GitHub at [https://gitlab.com/Vivek03t/rest-countries](https://gitlab.com/Vivek03t/rest-countries).


## Features

- View a list of countries with basic information.
- Search for countries by name.
- Filter countries by region.
- Filter by subregion.
- Sort by area ascending or descending.
- Sort by population ascending or descending.
- View detailed information about a selected country.
- View bordering countries.
- Toggle between light and dark themes.

## Technologies Used

- [React](https://reactjs.org/)
- [Vite](https://vitejs.dev/)
- [Bootstrap](https://getbootstrap.com/)
- HTML
- CSS
- JavaScript

## Getting Started

To get a local copy of this project up and running, follow these steps:

1. **Clone the repository:**

   ```bash
   git clone https://gitlab.com/Vivek03t/rest-countries
   ```
2. **Navigate to the project directory:**
   ```bash
   cd rest-countries-app
   ```
3. **Install dependencies:**
     ```bash
     npm install
     ```
4. **Run the development server:**
    ```bash
    npm run dev
    ```
5. **Open your browser:**
   Your app should now be running at http://localhost:5173.


## Usage

- On the homepage, you can see a list of countries.
- Use the search bar to look for a specific country by name.
- Filter countries by region using the dropdown menu.
- Filter countries by subregion using the dropdown menu.
- Sort countries by area using the dropdown menu.
- Sort countries by population using the dropdown menu.
- Click on a country card to view more details about that country.
- Click on a border country to view more details about the country.
- Toggle between light and dark themes by clicking the moon/sun icon in the top right corner.   
  
## Acknowledgments

Thanks to Rest Countries API for providing the country data used in this project.  